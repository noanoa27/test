import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

//Deployment
import { environment } from './../environments/environment';
import { Http , Headers } from '@angular/http';
@Injectable()
export class Test1Service {
  [x: string]: any;


  getUsersFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/users').valueChanges();
  }

  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }


}
