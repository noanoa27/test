import { Component, OnInit } from '@angular/core';
import { Test1Service } from "../test1.service";


@Component({
  selector: 'usersfire',
  templateUrl: './usersfire.component.html',
  styleUrls: ['./usersfire.component.css']
})
export class UsersfireComponent implements OnInit {

  users;

  constructor(private service:Test1Service) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(response=>{
        console.log(response);
        this.users = response;
    });
  }

}

