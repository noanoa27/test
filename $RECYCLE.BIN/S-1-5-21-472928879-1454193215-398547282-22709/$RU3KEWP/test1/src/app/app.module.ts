import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { UsersfireComponent } from './usersfire/usersfire.component';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    NotFoundComponent,
    UsersfireComponent
  ],
  imports: [
    BrowserModule,
     HttpModule,
     FormsModule,
      ReactiveFormsModule,
      AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path: 'usersfire', component: UsersfireComponent},
      {path: '**', component:NotFoundComponent}
         ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
